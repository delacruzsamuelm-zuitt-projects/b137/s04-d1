package b137.delacruz.s04d1;

public class Main {

    public static void main(String[] args) {
        Car firstCar = new Car();

        firstCar.setName("Fortuner");
        firstCar.setBrand("Toyota");
        firstCar.setYearOfMake(2021);

        firstCar.drive();

        Car secondCar = new Car("Territory", "Ford", 2010);

        secondCar.drive();
    }
}

package b137.delacruz.s04d1;

// Classes, Objects, Instance
// Properties
// Constructor
// Getters
// Setters
// Methods

public class Car {

    // Properties
    private String name;
    private String brand;
    private int yearOfMake;

    // Constructor

    // Parameterized Constructor
    public Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
    }

    // Empty Constructor
    public Car() {}

    // Getters
    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public int getYearOfMake() {
        return yearOfMake;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }

    public void setBrand(String newBrand) {
        this.brand = newBrand;
    }

    public void setYearOfMake(int newYearOfMake) {
        this.yearOfMake = newYearOfMake;
    }

    // Methods
    public void drive() {
        System.out.println("Driving the " + this.brand + " " + this.name + " year of make " + this.yearOfMake);
    }

}
